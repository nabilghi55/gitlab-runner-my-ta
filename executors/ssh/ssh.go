package ssh

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strconv"
	"strings"
	"time"

	"gitlab.com/gitlab-org/gitlab-runner/common"
	"gitlab.com/gitlab-org/gitlab-runner/executors"
	"gitlab.com/gitlab-org/gitlab-runner/helpers/ssh"
)

type executor struct {
	executors.AbstractExecutor
	sshCommand ssh.Client
	name       string
	host       string
	actions    map[string]bool
}

func findVariable(variables common.JobVariables, key string) string {
	for _, variable := range variables {
		if variable.Key == key {
			return variable.Value
		}
	}
	return ""
}

func findVariableFallback(variables common.JobVariables, key string, fallback string) string {
	res := findVariable(variables, key)
	if res == "" {
		return fallback
	}
	return res
}

func execContainersCommand(s *executor, cmd string) error {
	s.sshCommand = ssh.Client{
		Config: *s.Config.SSH,
		Stdout: s.Trace,
		Stderr: s.Trace,
	}

	fmt.Println("+ " + cmd)

	s.sshCommand.Config.User = "containers"

	err := s.sshCommand.Connect()
	if err != nil {
		return err
	}

	s.sshCommand.Exec(cmd)
	s.sshCommand.Cleanup()
	return nil
}

func (s *executor) Prepare(options common.ExecutorPrepareOptions) error {
	err := s.AbstractExecutor.Prepare(options)
	if err != nil {
		return fmt.Errorf("prearing AbstractExecutor: %w", err)
	}

	s.Println("Using SSH executor...")
	if s.BuildShell.PassFile {
		return errors.New("SSH doesn't support shells that require script file")
	}

	if s.Config.SSH == nil {
		return errors.New("missing SSH configuration")
	}

	s.Debugln("Starting SSH command...")

	variables := s.Build.GetAllVariables()
	projectPath := findVariable(variables, "CI_PROJECT_PATH_SLUG")
	buildRef := findVariable(variables, "CI_BUILD_REF_SLUG")
	name := fmt.Sprintf("%s-%s", projectPath, buildRef)
	s.name = name

	gpuRegex := variables.Get("CI_CM_GPU_REGEX")
	gpuCount, _ := strconv.Atoi(variables.Get("CI_CM_GPU_COUNT"))

	prefix := "container-manager-state"
	os.Mkdir("container-manager-state", 0700)
	stateFilePath := path.Join(prefix, name)

	currentHostRaw, _ := ioutil.ReadFile(stateFilePath)
	currentHost := string(currentHostRaw)
	if currentHost != "" {
		s.host = currentHost
	}

	if s.actions["rm-before"] && s.host != "" {
		execContainersCommand(s, "rm "+name)
		s.host = ""
		os.Remove(stateFilePath)
	}

	// decide host
	if s.host == "" {
		host, err := executors.GetGPUHost(gpuRegex, gpuCount)
		if err != nil {
			return err
		}
		s.host = host
		ioutil.WriteFile(stateFilePath, []byte(s.host), 0755)
	}
	s.Config.SSH.Host = s.host + ".mitre.org"
	s.BuildLogger.Infoln(name + "@" + s.host)

	rawActions := findVariable(variables, "CI_CM_ACTIONS")
	actions := make(map[string]bool)
	for _, action := range strings.Split(rawActions, ",") {
		actions[action] = true
	}
	s.actions = actions

	if s.actions["rm-before"] {
		execContainersCommand(s, "rm "+name)
	}

	if s.actions["create"] {
		execContainersCommand(s, "create --image ubuntu/18.04/docker "+name)
		time.Sleep(10 * time.Second)
	}

	shareProject := findVariable(variables, "CI_CM_SHARE_PROJECT")
	if s.actions["share"] {
		execContainersCommand(s, fmt.Sprintf("share --project %s --username %s %s", shareProject, "ci", name))
	}

	token := variables.Get("CI_CM_PROJECT_TOKEN")
	if s.actions["attach"] && token != "" {
		projects, err := executors.DecryptProjectToken(token)
		if err != nil {
			s.BuildLogger.Errorln("Error loading project token", err)
			return err
		}
		for _, project := range projects {
			cmd := fmt.Sprintf("project attach --project %s %s", project, name)
			execContainersCommand(s, cmd)
		}
	}

	if s.actions["rm-after"] {
		os.Remove(stateFilePath)
	}

	// Create SSH command
	s.sshCommand = ssh.Client{
		Config: *s.Config.SSH,
		Stdout: s.Trace,
		Stderr: s.Trace,
	}

	s.sshCommand.Config.User = name

	if gpuCount > 0 {
		err = s.sshCommand.Connect()
		if err != nil {
			return err
		}
		command := s.BuildShell.GetCommandWithArguments()
		cmd := fmt.Sprintf("gpu get --lock %d", gpuCount)
		err := s.sshCommand.Run(s.Context, ssh.Command{
			Environment: s.BuildShell.Environment,
			Command:     command,
			Stdin:       cmd,
		})
		if err != nil {
			return err
		}
		s.sshCommand.Cleanup()
	}

	err = s.sshCommand.Connect()
	if err != nil {
		return fmt.Errorf("ssh command Connect() error: %w", err)
	}

	return nil
}

func (s *executor) Run(cmd common.ExecutorCommand) error {
	err := s.sshCommand.Run(cmd.Context, ssh.Command{
		Environment: s.BuildShell.Environment,
		Command:     s.BuildShell.GetCommandWithArguments(),
		Stdin:       cmd.Script,
	})
	if _, ok := err.(*ssh.ExitError); ok {
		err = &common.BuildError{Inner: err}
	}
	return err
}

func (s *executor) Cleanup() {
	s.sshCommand.Cleanup()
	if s.actions["rm-after"] {
		execContainersCommand(s, "rm "+s.name)
	}
	s.AbstractExecutor.Cleanup()
}

func init() {
	options := executors.ExecutorOptions{
		DefaultCustomBuildsDirEnabled: false,
		DefaultBuildsDir:              "builds",
		DefaultCacheDir:               "cache",
		SharedBuildsDir:               true,
		Shell: common.ShellScriptInfo{
			Shell:         "bash",
			Type:          common.LoginShell,
			RunnerCommand: "gitlab-runner",
		},
		ShowHostname: true,
	}

	creator := func() common.Executor {
		return &executor{
			AbstractExecutor: executors.AbstractExecutor{
				ExecutorOptions: options,
			},
		}
	}

	featuresUpdater := func(features *common.FeaturesInfo) {
		features.Variables = true
		features.Shared = true
	}

	common.RegisterExecutorProvider("ssh", executors.DefaultExecutorProvider{
		Creator:          creator,
		FeaturesUpdater:  featuresUpdater,
		DefaultShellName: options.Shell.Shell,
	})
}
