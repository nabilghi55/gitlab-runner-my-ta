package docker

import (
	"fmt"
	"strconv"
	"sync"
	"time"
)

type CMConnection struct {
	path           string
	lastActiveTime time.Time
	cleanupFn      func()
	jobCount       int
	inactive       bool
}

type CMConnectionManager struct {
	lock        sync.Mutex
	connections map[string]*CMConnection
}

func MakeCMConnectionManager() *CMConnectionManager {
	m := &CMConnectionManager{
		connections: make(map[string]*CMConnection),
	}
	go m.cleanerLoop()
	return m
}

// GetDockerPath returns a path to the docker socket and a cleanup function.
// It determines whether we can reusue an existing connection, creates one
// if needed, and returns it
//
func (m *CMConnectionManager) GetDockerPath(e *executor) (string, func()) {
	vars := e.Build.GetAllVariables()
	count, _ := strconv.Atoi(vars.Get("CI_CM_GPU_COUNT"))
	if count > 0 {
		return SetupContainerManager(e)
	}

	id := vars.Get("CI_PIPELINE_ID")
	uniqueKey := vars.Get("CI_CM_UNIQUE_KEY")
	id = fmt.Sprintf("%s%s", id, uniqueKey)

	m.lock.Lock()
	defer m.lock.Unlock()
	_, ok := m.connections[id]
	if !ok {
		path, cleanupFn := SetupContainerManager(e)

		conn := &CMConnection{
			path:      path,
			cleanupFn: cleanupFn,
		}
		m.connections[id] = conn
	}

	conn, _ := m.connections[id]
	conn.lastActiveTime = time.Now()
	conn.jobCount++
	path := conn.path
	cleanup := func() {
		m.lock.Lock()
		defer m.lock.Unlock()
		conn, ok := m.connections[id]
		if ok {
			conn.jobCount--
		} else {
			fmt.Println("connection already closed")
		}
	}

	return path, cleanup
}

// cleanerLoop removes unused containers every 15 seconds
// during the first pass, the connection will be marked inactive
// if same state next loop, connection will be terminated
func (m *CMConnectionManager) cleanerLoop() {
	for {
		time.Sleep(time.Second * 15)
		m.lock.Lock()
		expireTime := time.Now().Add(-time.Hour)
		minExpireTime := time.Now().Add(-time.Minute)
		for id, conn := range m.connections {
			lastActiveTime := conn.lastActiveTime
			isPipelineDone := conn.jobCount == 0 && lastActiveTime.After(minExpireTime)
			isPipelineExpired := lastActiveTime.Before(expireTime)
			if isPipelineDone || isPipelineExpired {
				if conn.inactive {
					go conn.cleanupFn()
					delete(m.connections, id)
				} else {
					conn.inactive = true
				}
			} else {
				conn.inactive = false
			}
		}
		m.lock.Unlock()
	}
}
