package executors

import (
	"os"
	"fmt"
	"encoding/json"
	"sort"
	"regexp"
	"errors"
	"net/http"
)

type Host struct {
	Hostname string
	Info struct {
		GPU map[string]string
	}
	Stats struct {
		CPU int
		Disk int
		Memory int
		GPU map[string] struct {
			Username string
			Container string
			Timestamp int64
		}
	}
}

type Info map[string] Host

type FlatInfo []Host

func (s *Info) flatten() FlatInfo {
	var res FlatInfo
	for _, value := range *s {
		res = append(res, value)
	}
	return res
}

func (s FlatInfo) filterDangerousUsage() FlatInfo {
	var res FlatInfo
	for _, value := range s {
		hostname := value.Hostname
		disk := value.Stats.Disk
		memory := value.Stats.Memory
		message := "Not considering %s because of high %s usage (%d%%)\n"
		if disk > 80 {
			fmt.Printf(message, hostname, "disk", disk)
			continue
		}
		if memory > 90 {
			fmt.Printf(message, hostname, "memory", disk)
			continue
		}
		res = append(res, value)
	}
	return res
}

func calcUtilWeight(stat Host) float32 {
	s := stat.Stats
	return float32(s.CPU) * 1.5 + float32(s.Memory) * 1.25 + float32(s.Disk) * .25
}

// sortByUtilization sorts FlatInfo by CPU Memory and disk utilization
// We weight CPU > Memory > Disk
func (s FlatInfo) sortByUtilization() FlatInfo {
	sort.Slice(s, func(i, j int) bool {
		siWeight := calcUtilWeight(s[i])
		sjWeight := calcUtilWeight(s[j])
		return siWeight < sjWeight
	})
	return s
}

func (s FlatInfo) flattenToHostnames() []string {
	var res []string
	for _, value := range s {
		res = append(res, value.Hostname)
	}
	return res
}

func (s FlatInfo) applyConstraints(gpuRegex string, gpuCount int) (FlatInfo, error) {
	var res FlatInfo
	for _, stat := range s {
		doesSatisfy, err := stat.doesSatisfyGPUConstraint(gpuRegex, gpuCount)
		if err != nil {
			return nil, err
		}
		if doesSatisfy {
			res = append(res, stat)
		}
	}
	return res, nil
}

func (h Host) countUnusedGPUs() int {
	unused := 0
	for _, gpu := range h.Stats.GPU {
		if gpu.Username == "" {
			unused++
		}
	}
	return unused
}

func (h Host) doesSatisfyGPUConstraint(gpuRegex string, gpuCount int) (bool, error) {
	r, err := regexp.Compile("(?i)" + gpuRegex)
	if err != nil {
		return false, err
	}
	
	// WARN: we only test the gpuRegex on the first GPU. Ignore systems that have different types of GPUs
	if ! r.Match([]byte(h.Info.GPU["0"])) {
		return false, nil
	}
	unusedGPUs := h.countUnusedGPUs()
	if unusedGPUs < gpuCount {
		return false, nil
	}
	
	return true, nil
}

func GetGPUHost(gpuRegex string, gpuCount int) (string, error) {
	server := os.Getenv("STATS_SERVER")
	res, err := http.Get(server + "/info")
	if err != nil {
		return "", err
	}

	var i Info
	err = json.NewDecoder(res.Body).Decode(&i)
	if err != nil {
		return "", err
	}
	flat := i.flatten()
	flat = flat.filterDangerousUsage()
	flat = flat.sortByUtilization()
	flat, err = flat.applyConstraints(gpuRegex, gpuCount)
	if err != nil {
		return "", err
	}
	if len(flat) > 0 {
		return flat[0].Hostname, nil
	}
	return "", errors.New("unable to satisfy resource constraints")
}